<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <link href="{{asset('css/formulir.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    <link rel="icon" href="{{asset('image/Untitleddd.png')}}">
    <title>Formulir Penyewaan</title>
</head>
<body>
@php
        use \App\Http\Controllers\SessionController;
        echo SessionController::navbar();
    @endphp
    <section>
        <div class="container">
            <div class="col-12 col-lg-10 justify-content-center mx-auto">
                <form action="{{route('formulirPost')}}" method="POST">
                    @csrf
                    <h2>Detail Penyewaan</h2>
                    <table>
                        <tr>
                            <th>Jenis Mobil</th>
                            <td>{{$mobil->nama}}</td>
                            
                        </tr>
                        <tr>
                            <th>Tanggal Pengambilan</th>
                            <td>
                                <input class="inputM browser-default" type="date" placeholder="Tanggal Pengambilan" name="pengambilan" id="date_ambil" oninput="tempCel(this.value)" onclick="">
                            </td>
                        </tr>
                        <tr>
                            <th>Tanggal Pengembalian</th>
                            <td>
                                <input class="inputM browser-default" type="date" placeholder="Tanggal Pengembalian" name="pengembalian" id="date_ngembaliin" oninput="tempCel2(this.value)">
                            </td>
                            <input type="number" name="id" value="{{$mobil->id}}" style="display: none">
                        </tr>
                    </table>

                    {{-- <p style="margin-bottom:15px;">Mobil : {{$mobil->nama}}</p>
                    <input type="number" name="id" value="{{$mobil->id}}" style="display: none">
                    <input class="inputM browser-default" type="date" placeholder="Tanggal Pengambilan" name="pengambilan" id="date_ambil" oninput="tempCel(this.value)" onclick="">
                    <br>
                    <input class="inputM browser-default" type="date" placeholder="Tanggal Pengembalian" name="pengembalian" id="date_ngembaliin"oninput="tempCel2(this.value)"> --}}

                    <p style="font-size: 11px; font-weight:bold; margin-bottom:30px;margin-top:5px;">*Tanggal Peminjaman dan Pengembalian <br>ditulis dengan format mm/dd/yyyy</p>

                    <p style="margin-bottom:15px ;">Harga Penyewaan : <span id="harga_mobil">NaN</span> x <span id="tes" name="tes">NaN</span> hari =  <span id="hasil_akhir">NaN</span> </p>
                    <button type="submit" class="prim-button">Kirim</button>
                </form>
            </div>
            
        </div>
    </section>
    



    <script>
        function tempCel(numbah_ambil){

            document.getElementById('tes').innerHTML=numbah_ambil;

        }
        function tempCel2(numbah_ambil){
            
            x = document.getElementById('tes').innerHTML.split("-");
            y = numbah_ambil.split("-");
            x = x[0]*365+x[1]*30+x[2];
            y = y[0]*365+y[1]*30+y[2];
            hasil = y-x;
            function formatCurrency(amount) {
                // Convert the number to a string and add IDR symbol
                let formattedAmount = 'Rp ' + amount.toFixed(0).replace(/\d(?=(\d{3})+$)/g, '$&,');
                return formattedAmount;
            }

            if (hasil<0){
                alert("gaboleh dibawah 0")
                document.getElementById('tes').innerHTML = 'NaN';
                document.getElementById('hasil_akhir').innerHTML = 'NaN';

            }else{
                document.getElementById('tes').innerHTML = hasil;
                hargaMobil = {{ $mobil['harga'] }};
                document.getElementById('harga_mobil').innerHTML=formatCurrency(hargaMobil);
                document.getElementById('hasil_akhir').innerHTML = formatCurrency(hasil*hargaMobil);
            }
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </script>
</body>
</html>
