<!doctype html>
<html lang="en">
<head>

    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Menu Utama</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
    {{-- <link rel="icon" href="{{mix('image/Untitleddd.png')}}"> --}}
    <link rel="stylesheet" href="{{ URL::asset('public/css/dashboardItems.css') }}" />

    {{--
    <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script> --}}
</head>
<body>
	@php
        use \App\Http\Controllers\SessionController;
        use Illuminate\Support\Str;
        echo SessionController::navbar();
    @endphp
    <style>
        .img-container{
            position: relative;
            text-align: center;
            color: white;
        }
        .top-right {
            position: absolute;
            top: 8px;
            right: 16px;
            color: black;
        }
        .card {
            border: none;
        }
        .card-body {
            border: 1px solid black;
            border-top: none; /* Remove top border */
            border-bottom-right-radius: 12px; /* Add bottom-right border radius */
            border-bottom-left-radius: 12px; /* Add bottom-left border radius */
        }
        .card-img-top{
            border-top-left-radius:12px;
            border-top-right-radius:12px;
            object-fit: cover;
            height: 200px;
        }
    </style>
    <section>
        <div class="container mt-4">
            <div class="row">
                <div class="col-lg-12">
                    <p>Hello, {{ auth()->user()->nama }}</p>
                </div>
            </div>
        </div>
    </section>
    <section>
        
        <div class="container mt-2">
            <div class="row">
                @foreach ($mobil as $mobil)
                    <form action="{{ route('formulir') }}" method="GET" class="col-6 col-md-6 col-lg-4" id="mobilForm{{ $mobil['id'] }}">
                        <div class="col">
                            <input name="id" style="display: none;" value="{{$mobil['id']}}" readonly>
                            <div class="card" style="width: auto;">
                                <button type="submit" class="btn" style="background: transparent; border: none; outline: none;">
                                    <div class="img-container">
                                        <img src="{{ $mobil['foto'] }}" class="card-img-top" alt="..." style="width:100%;">
                                        <div class="top-right">Sisa {{ $mobil['jumlah'] }}</div>
                                    </div>
                                    <div class="card-body">
                                        <div class="row text-start">
                                            <div class="col"><h5 class="my-auto">{{ Str::limit($mobil['nama'], 23) }}</h5></div>
                                            <div class="col-md-4 ms-auto text-end"><p>{{ $mobil['mesin'] }} cc </p></div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">{{ 'Rp ' . number_format($mobil['harga'], 0, ',', '.') }}</div>
                                            <div class="col-md-4 ms-auto"></div>
                                        </div>
                                    </div>
                                </button>
                            </div>
                        </div>
                    </form>

                @endforeach

                <script>
                    function submitForm(mobilId) {
                        document.getElementById('mobilForm' + mobilId).submit();
                    }
                </script>
            </div>
        </div>
    </section>
        @if(auth()->check())
            {{-- <p>Hello, {{ auth()->user()->nama }}</p> --}}
            {{-- <h1>Mobil yang tersedia</h1>
            <div class="item" data-aos="fade-up">
                @foreach ($mobil as $mobil)
                <form action="{{route('formulir')}}" method="GET">

                    <div class='cards'>
                    <img src="{{$mobil['foto']}}" class="gambarr"/>
                    <input type="submit" value="AAAAAA" class="kartuMobil">

                    <input name="id" style="display: none;" value="{{$mobil['id']}}" readonly>

                        <div class='card-title'>
                            <p>Sisa {{$mobil['jumlah']}}</p>
                        </div>
                        <div class='card-car-name'>
                            <p>{{$mobil['nama']}} </p>
                        </div>
                        <div class='card-car-cc'>
                            <p>{{$mobil['mesin']}} cc </p>
                        </div>
                        <div class='card-car-price'>
                            <p>{{$mobil['harga']}} <span class='price-hari'>/hari</span></p>
                        </div>
                    </div>
                </form>

                @endforeach

            </div> --}}
        @else
            <p>Hello, Stranger</p>
        @endif

        <script>
            AOS.init();
       </script>
       <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.10.2/dist/umd/popper.min.js"
        integrity="sha384-7+zCNj/IqJ95wo16oMtfsKbZ9ccEh31eOz1HGyDuCQ6wgnyJNSYdrPa03rtR1zdB" crossorigin="anonymous">
    </script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.min.js"
        integrity="sha384-QJHtvGhmr9XOIpI6YVutG+2QOK9T+ZnN4kzFN1RtK3zEFEIsxhlmWl5/YESvpZ13" crossorigin="anonymous">
    </script>
</body>
</html>
