<!doctype html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Profile</title>
    <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600&display=swap" rel="stylesheet">
    <link rel="icon" href="{{asset('image/Untitleddd.png')}}">
    <link href="{{asset('css/profil.css')}}" rel="stylesheet" type="text/css" />
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" />
</head>

<body>
    {{-- @include('Partials.navbarPemilik') --}}
    @php
    use \App\Http\Controllers\SessionController;
    echo SessionController::navbar();
    @endphp

    <div class="container">
        <div class="profile-card mt-5">
            <div class="card">
                <div class="card-header">
                    Profil Pengguna
                </div>
                <div class="card-body">
                    <h5 class="card-title fw-bold">Halo, {{ auth()->user()->nama }}</h5>
                    <p class="card-text">Tolong pastikan data anda dibawah sudah benar</p>
                        <div id="fixed">
                            @if(auth()->check())
                            <table style="border-spacing: 10px;">
                                <tr class="">
                                    <td class="label ">Nama</td>
                                    <td class="inputProfile ubahProfile ">{{ auth()->user()->nama }}</td>
                                </tr>
                                <tr>
                                    <td class="label ">NIK</td>
                                    <td class="inputProfile ubahProfile ">{{ auth()->user()->nik }}</td>
                                </tr>
                                <tr>
                                    <td class="label ">Telepon</td>
                                    <td class="inputProfile ubahProfile ">{{ auth()->user()->telepon }}</td>
                                </tr>
                                <tr>
                                    <td class="label ">Kota Asal</td>
                                    <td class="inputProfile ubahProfile ">{{ auth()->user()->kota_asal }}</td>
                                </tr>
                            </table>
                            @else
                            <meta http-equiv="refresh" content="0; URL={{ route('sessCreate') }}" />
                            @endif
                        </div>
                        <div id="edit" style="display: none;">
                            <form method="POST" action="{{ route('profUpdate') }}" id="patch" class="center">
                                {{ csrf_field() }}
                                <table class="center">
                                    <tr class="browser-default">
                                        <td class="label browser-default">Nama</td>
                                        <td class="inputProfile browser-default"><input type="text" class="form-control browser-default" id="nama" name="nama" value="{{ auth()->user()->nama }}"></td>
                                    </tr>
                                    <tr class="browser-default">
                                        <td class="label browser-default">NIK</td>
                                        <td class="inputProfile browser-default"><input type="text" class="form-control browser-default" id="nik" name="nik" value="{{ auth()->user()->nik }}"></td>
                                    </tr>
                                    <tr class="browser-default">
                                        <td class="label browser-default">Telepon</td>
                                        <td class="inputProfile browser-default"><input type="text" class="form-control browser-default" id="telepon" name="telepon" value="{{ auth()->user()->telepon }}"></td>
                                    </tr>
                                    <tr class="browser-default">
                                        <td class="label browser-default">Kota Asal</td>
                                        <td class="inputProfile browser-default"><input type="text" class="form-control browser-default" id="kota_asal" name="kota_asal" value="{{ auth()->user()->kota_asal }}"></td>
                                    </tr>
                                </table>

                            </form>
                        </div>
                        <br>
                        <div class="center">
                        <div>
                            <button id="hide" onclick="edit()" type="button" class="btn btn-primary">Edit</button>
                            <button style="cursor:pointer; display: none;" form="patch" type="submit" id="save" onclick="save()" class="btn btn-success">Save</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script>
        function edit() {
            document.getElementById('hide').style.display = 'none';
            document.getElementById('fixed').style.display = 'none';
            document.getElementById('edit').style.display = 'block';
            document.getElementById('save').style.display = 'inline-block';
            document
        }

        function save() {
            document.getElementById('hide').style.display = 'inline-block';
            document.getElementById('save').style.display = 'none';
            document.getElementById('edit').style.display = 'none';
            document.getElementById('fixed').style.display = 'block';
        }
    </script>
</body>

</html>