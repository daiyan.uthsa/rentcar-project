<!DOCTYPE html>
<html lang="en">
<head>
        {{-- <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous" /> --}}
        {{-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"> --}}
        {{-- <link href="https://fonts.googleapis.com/css2?family=Montserrat:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <title>Menu</title> --}}
        <link href="{{asset('css/navbar.css')}}" rel="stylesheet" type="text/css" />
        </head>
        <body>
                @if( auth()->check() )
                {{-- <nav class="nav browser-default">
                        <div class="nav-wrapper browser-default">
                                <a href="{{route('dashboard')}}" class="brand-logo">Car<span class='sred'>Rent</a>
                                <a href="{{route('profView')}}"class="ungu" style="padding-left: 220px">Profil</a>
                                <a href="{{route('userCreateHist')}}" class="ungu" style="padding-left: 40px">Riwayat</a>
                                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons menu">menu</i></a>
                                <ul class="right hide-on-med-and-down other_nav">
                                <li><a href="{{route('sessDestroy')}}" class="ungu keluar">Keluar</a></li>
                                </ul>
                                <ul class="side-nav" id="mobile-demo">
                                <li><a href="{{route('profView')}}"class="ungu">Profil</a></li>
                                <li><a href="{{route('userCreateHist')}}"class="ungu">Riwayat</a></li>
                                <li><a href="{{route('sessDestroy')}}"class="ungu keluar">Keluar</a></li>
                                </ul>
                        </div>
                </nav> --}}
                {{-- <nav class="navbar navbar-expand-lg bg-body-tertiary">
                        <div class="container-fluid">
                                <a class="navbar-brand" href="{{route('dashboard')}}">
                                <img src="{{asset('image/Untitleddd.png')}}" alt="semina" />
                                </a>
                                <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
                                data-bs-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false"
                                aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                                </button>
                                <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
                                <div class="navbar-nav mx-auto my-3 my-lg-0">
                                        <a class="nav-link" href="{{route('profView')}}">Profil</a>
                                        <a class="nav-link" href="{{route('userCreateHist')}}">Riwayat</a>
                                </div>
                                <div class="d-grid">
                                        <a class="btn-navy" href="{{route('sessDestroy')}}">
                                        Keluar
                                        </a>
                                </div>
                                </div>
                        </div>
                </nav> --}}
                <nav class="navbar navbar-expand-lg bg-dark">
                   <div class="container-fluid">
                        <a href="{{route('dashboard')}}" class="navbar-brand">
                                Car<span>
                                Rent
                        </a>
                        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512"><!--! Font Awesome Free 6.4.2 by @fontawesome - https://fontawesome.com License - https://fontawesome.com/license (Commercial License) Copyright 2023 Fonticons, Inc. --><path d="M0 96C0 78.3 14.3 64 32 64H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 128 0 113.7 0 96zM0 256c0-17.7 14.3-32 32-32H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32c-17.7 0-32-14.3-32-32zM448 416c0 17.7-14.3 32-32 32H32c-17.7 0-32-14.3-32-32s14.3-32 32-32H416c17.7 0 32 14.3 32 32z"/></svg>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                          <li class="nav-item">
                                <a class="nav-link" href="{{route('profView')}}">Profil</a>
                          </li>
                                <li class="nav-item">
                                <a class="nav-link" href="{{route('userCreateHist')}}">Riwayat</a>
                                </li>
                        </ul>
                        <div class="d-flex">
                            <a class="btn-navy" href="{{route('sessDestroy')}}">
                                        Keluar
                                </a>
                        </div>
                        </div>
                  </div>
                </nav>
                @else
                        <a href="{{route('sessCreate')}}">Log In</a>
                        <a href="{{route('regsCreate')}}">Register</a>
                @endif
                
  <!--Import jQuery before materialize.js-->
  <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script>
  <script type="text/javascript" src="js/materialize.min.js"></script>
    <script>
        $(".button-collapse").sideNav();
        $(document).ready(function(){
        $('.carousel').carousel({});
    });
        $('.carousel.carousel-slider').carousel({
            fullWidth:true,
            indicators:true,
        });

    </script>
        </body>
</html>


