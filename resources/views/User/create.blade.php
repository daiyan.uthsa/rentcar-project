<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,500;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,500;1,600;1,700;1,800;1,900&display=swap" rel="stylesheet">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"> -->
    <!-- <link href="{{asset('css/semua.css')}}" rel="stylesheet" type="text/css" /> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link href="{{asset('css/create.css')}}" rel="stylesheet" type="text/css" />
    <link rel="icon" href="{{asset('image/Untitleddd.png')}}">
</head>
<body>
    @if ($errors->any())
        <script>alert('Data yang anda masukkan telah digunakan')</script>
    @endif


    <div class="container-fluid vh-100">
        <div class="row vh-100">
            <div class="col-7 bg-image">
                
            </div>
            <div class="section-2 col-5">
                <div class="form-container mx-auto rounded p-3 bg-light">
                    <h2 class="text-center">Buat Akun</h2>
                    <p class="text-center">Daftarkan diri anda untuk mulai menggunakan CarRent</p>

                        <form method="POST" action="{{ route('regsStore') }}">
                                {{csrf_field()}}
                                
                                <div class="">
                                        <label for="nama">Nama</label>
                                        <input type="text" class="form-control input" id="nama" name="nama" placeholder="Nama" required>
                                    </div>
                                <div class="">
                                        <label for="nik">NIK</label>
                                        <input type="text" class="form-control input" id="nik" name="nik" placeholder="NIK" required>
                                    </div>
                                
                                <div class="form-row">
                                    <div class="form-col">
                                        <label for="kota_asal">Kota Asal</label>
                                        <input type="text" class="form-control input" id="kota_asal" name="kota_asal" placeholder="Kota asal" required>
                                    </div>
                                    <div class="form-col">
                                        <label for="telepon">Nomor HP</label>
                                        <input type="text" class="form-control input" id="telepon" name="telepon" placeholder="Nomor HP" required>
                                    </div>
                                </div>
                                
                                <div class="mt-2">
                                    <label for="password">Password</label>
                                    <input type="password" class="form-control input" id="password" name="password" placeholder="Password" required>
                                </div>
                                <div class="mt-4">
                                    <button style="cursor:pointer" type="submit" class="btn btn-primary w-100">Sign Up</button>
                                </div>
                        </form>
                        
                        <p class="mt-2 text-center">Sudah memiliki akun? <a href="{{route('sessCreate')}}">Masuk</a></p>
                </div>
            </div>
        </div>
        
        
    </div>

    
    @section('scripts')
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js"></script> 
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
    @stop
</body>
</html>
