<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css"> -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <link rel="icon" href="{{asset('image/Untitleddd.png')}}">
    <link rel="icon" href="{{asset('image/carOrder.png')}}">
    <link href="{{asset('css/login.css')}}" rel="stylesheet" type="text/css" />
  
</head>
<body>
  
<div class="container-fluid vh-100 ">
    <div class="row h-100 bg-light">
      <div class="col-7 image-background"></div>
      <div class="section-2 col-5 my-auto">
        <h2 class="text-center fw-bolder">Selamat Datang di CarRent</h2>
        <!-- Form Normal -->
        <form method="POST" action="{{route('sessStore')}}">
            {{ csrf_field() }}
            <div class="form-floating mx-auto mt-5">
                <input type="text" class="form-control " id="telepon" name="telepon" placeholder="Nomor HP" required>
                <label for="telepon">Nomor HP</label>
            </div>

            <div class="form-floating mt-2">
                <input type="password" class="form-control" id="password" name="password" placeholder="Password" required>
                <label for="password">Password</label>
            </div>

            <div class="d-flex">
                <button style="cursor:pointer" type="submit" class="btn btn-primary w-100 mt-5">Login</button>
            </div>
        </form>

        <p class="text-center mt-3">Belum Punya Akun? <a href="{{route('regsCreate')}}">Klik Disini</a></p>
        
      </div>
  </div>
  </div>



  @section('scripts')
  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous"></script>
  @stop
</body>
</html>
